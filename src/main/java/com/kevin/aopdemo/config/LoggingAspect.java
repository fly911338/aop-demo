package com.kevin.aopdemo.config;

import com.kevin.aopdemo.model.Log;
import com.kevin.aopdemo.repository.LoggerRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;

import java.time.Instant;

@Aspect
@Component
@AllArgsConstructor
@Slf4j
public class LoggingAspect {

    private final LoggerRepository loggerRepository;

    // capture controller package and sub-package all method
    @Pointcut("execution(* com.kevin.aopdemo.controller.*.*(..))")
    public void pointCutLoginController() {}

    // capture controller package and sub-package
    @Pointcut("within(com.kevin.aopdemo.controller..*)")
    public void controllerLog() {}

    @Before("controllerLog()")
    public void beforeLog() {
        log.info("this is @Before...");
    }

    @After("controllerLog()")
    public void afterLog() {
        log.info("this is @After...");
    }

    @AfterReturning(pointcut = "controllerLog()", returning = "returnValue")
    public void afterReturnLog(Object returnValue) {
        log.info("this is @AfterReturning start...");
        if (returnValue != null) {
            log.info("capture return value...");
            log.info("returnValue is: " + returnValue.toString());
        }
        log.info("this is @AfterReturning end...");
    }

    @Around("controllerLog()")
    public Object aroundLog(ProceedingJoinPoint proceedingJoinPoint) {
          log.info("this is @Around start...");
        // before handle
        // get capture method name
        String methodName = proceedingJoinPoint.getSignature().getName();
        String className = proceedingJoinPoint.getTarget().toString();
        Object obj = null;

        // execution target method
        try {
            // obj is return value
            // in this case is ResponseEntity object
            obj = proceedingJoinPoint.proceed();
            if (obj != null) {
                log.info(obj.toString());
            }
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        // after handle
        loggerRepository.addLog(new Log(methodName, className, Instant.now()));
        log.info("this is @Around end...");
        return obj;
    }

}
