package com.kevin.aopdemo.config;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
@Slf4j
public class AbstractAspect {

    @Pointcut("target(com.kevin.aopdemo.controller.AbstractController)")
    public void pointcut() {}

    @Before("pointcut()")
    public void before() {
        log.info("@before...");
    }
}
