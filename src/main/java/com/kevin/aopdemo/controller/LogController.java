package com.kevin.aopdemo.controller;

import com.kevin.aopdemo.service.LogService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/log")
@AllArgsConstructor
public class LogController {

    private final LogService logService;

    @GetMapping("all")
    public void showAllLog() {
        logService.showAllLog();
    }
}
