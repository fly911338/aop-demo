package com.kevin.aopdemo.controller;

import org.springframework.http.ResponseEntity;

public interface AbstractController {

    ResponseEntity<String> get(String name);
}
