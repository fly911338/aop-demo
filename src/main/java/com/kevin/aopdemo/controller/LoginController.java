package com.kevin.aopdemo.controller;

import com.kevin.aopdemo.model.Account;
import com.kevin.aopdemo.service.LoginService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/account")
@AllArgsConstructor
@Slf4j
public class LoginController {

    private final LoginService loginService;

    @PostMapping("register")
    public ResponseEntity<String> register(@RequestBody Account account) {
        log.info("this is controller level register method");
        loginService.register(account);
        return new ResponseEntity("register success !!", HttpStatus.OK);
    }

    @PostMapping("login")
    public ResponseEntity<String> login(@RequestBody Account account) {
        log.info("this is controller level login method");
        boolean isLogin = loginService.login(account);
        return new ResponseEntity<>("login " + isLogin, HttpStatus.OK);
    }

    @DeleteMapping("delete")
    public ResponseEntity<String> deleteAccount(@RequestBody Account account) {
        log.info("this is controller level delete method");
        boolean isDelete = loginService.deleteAccount(account);
        return new ResponseEntity<>(
                String.format("account: %s is delete: %s", account.getUsername(), isDelete),
                HttpStatus.OK);

    }
}
