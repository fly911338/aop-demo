package com.kevin.aopdemo.controller;

import com.kevin.aopdemo.model.Product;
import com.kevin.aopdemo.service.ProductService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/v1/product")
@AllArgsConstructor
@Slf4j
public class ProductController {

    private final ProductService productService;

    @PostMapping("add")
    public ResponseEntity<String> addProduct(@RequestBody Product product) {
        log.info("this is controller level addProduct method");
        productService.addProduct(product);
        return new ResponseEntity("add product success !!", HttpStatus.OK);
    }

    @GetMapping("all")
    public ResponseEntity<List<Product>> getAllProducts() {
        log.info("this is controller level getAllProducts method");
        List<Product> products = productService.getAllProducts();
        return new ResponseEntity(products, HttpStatus.OK);
    }

    @GetMapping("{productName}")
    public ResponseEntity<Product> getProduct(@PathVariable String productName) {
        log.info("this is controller level getProduct method");
        Product product = productService.getProduct(productName);
        return new ResponseEntity(product, HttpStatus.OK);
    }

    @PutMapping("update")
    public ResponseEntity<String> updateProduct(@RequestBody Product product) {
        log.info("this is controller level updateProduct method");
        boolean isUpdateSuccess = productService.updateProduct(product);
        return new ResponseEntity(String.format("product: %s update: %s", product.getName(), isUpdateSuccess), HttpStatus.OK);
    }

    @DeleteMapping("{productName}")
    public ResponseEntity<String> deleteProduct(@PathVariable String productName) {
        log.info("this is controller level deleteProduct method");
        boolean isDelete = productService.deleteProduct(productName);
        return new ResponseEntity(String.format("product: %s delete: %s", productName, isDelete), HttpStatus.OK);
    }
}
