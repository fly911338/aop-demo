package com.kevin.aopdemo.repository;

import com.kevin.aopdemo.model.Account;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

// mock database
@Component
@Slf4j
public class LoginRepository {

    private List<Account> accountList = new ArrayList<>();

    public void register(Account account) {
        log.info("this is repository level register method");
        accountList.add(account);
    }

    public boolean findUser(Account account) {
        log.info("this is repository level login method");
        return accountList.stream()
                .filter(a -> a.getUsername().equals(account.getUsername()) && a.getPassword().equals(account.getPassword()))
                .findAny().isPresent();
    }

    public boolean delete(Account account) {
        log.info("this is repository level delete method");
        Optional<Account> optionalAccount = accountList.stream()
                .filter(a -> a.getUsername().equals(account.getUsername()) && a.getPassword().equals(account.getPassword()))
                .findAny();
        if (optionalAccount.isPresent()) {
            Account account1 = optionalAccount.get();
            accountList.remove(account1);
            return true;
        } else {
            return false;
        }
    }
}
