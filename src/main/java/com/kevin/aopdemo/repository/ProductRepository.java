package com.kevin.aopdemo.repository;

import com.kevin.aopdemo.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Slf4j
public class ProductRepository {

    List<Product> products = new ArrayList<>();

    public void save(Product product) {
        products.add(product);
    }

    public List<Product> findAll() {
        return products;
    }

    public Product findByUser(String productName) {
        Optional<Product> optionalProduct = products.stream()
                .filter(p -> p.getName().equals(productName))
                .findAny();
        return optionalProduct.orElseThrow(()
                -> new RuntimeException("not found product..."));
    }

    public boolean update(Product product) {
        Product product1 = products.stream()
                .filter(p -> p.getName().equals(product.getName()))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("not found product update failure..."));
        product1.setName(product.getName());
        product1.setAmount(product.getAmount());
        return true;
    }

    public boolean delete(String productName) {
        Product product1 = products.stream()
                .filter(p -> p.getName().equals(productName))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("not found product delete failure..."));
        products.remove(product1);
        return true;
    }
}
