package com.kevin.aopdemo.repository;

import com.kevin.aopdemo.model.Log;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Slf4j
public class LoggerRepository {
    List<Log> logs = new ArrayList<>();

    public void addLog(Log log) {
        logs.add(log);
    }

    public void showAllLog() {
        if (logs.size() == 0) {
            System.out.println("not found any log...");
        } else {
            logs.forEach(System.out::println);
        }
    }

}
