package com.kevin.aopdemo.other.controller;

import com.kevin.aopdemo.controller.AbstractController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/hello")
public class HelloController implements AbstractController
{
    @Override
    @GetMapping("get")
    public ResponseEntity<String> get(String name) {
        return new ResponseEntity<>("this is hello controller", HttpStatus.OK);
    }
}
