package com.kevin.aopdemo.service;

import com.kevin.aopdemo.model.Product;
import com.kevin.aopdemo.repository.ProductRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
@Slf4j
public class ProductService {

    private final ProductRepository productRepository;

    public void addProduct(Product product) {
        log.info("this is service level addProduct method");
        productRepository.save(product);
    }

    public List<Product> getAllProducts() {
        log.info("this is service level getAllProducts method");
        return productRepository.findAll();
    }

    public Product getProduct(String productName) {
        log.info("this is service level getProduct method");
        return productRepository.findByUser(productName);
    }

    public boolean updateProduct(Product product) {
        log.info("this is service level updateProduct method");
        return productRepository.update(product);
    }

    public boolean deleteProduct(String productName) {
        log.info("this is service level deleteProduct method");
        return productRepository.delete(productName);
    }
}
