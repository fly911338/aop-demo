package com.kevin.aopdemo.service;

import com.kevin.aopdemo.model.Account;
import com.kevin.aopdemo.repository.LoginRepository;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
@Slf4j
public class LoginService {

    private final LoginRepository loginRepository;

    public void register(Account account) {
        log.info("this is service level register method");
        loginRepository.register(account);
    }

    public boolean login(Account account) {
        log.info("this is service level login method");
        return loginRepository.findUser(account);
    }

    public boolean deleteAccount(Account account) {
        log.info("this is service level delete method");
        return loginRepository.delete(account);
    }
}
