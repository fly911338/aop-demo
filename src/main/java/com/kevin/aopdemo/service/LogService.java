package com.kevin.aopdemo.service;

import com.kevin.aopdemo.repository.LoggerRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class LogService {

    private final LoggerRepository loggerRepository;

    public void showAllLog() {
        loggerRepository.showAllLog();
    }
}
